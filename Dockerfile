FROM python:alpine

ARG VERSION=v2.2
ARG TARGETPLATFORM
LABEL maintainer="Jay MOULIN <jaymoulin@gmail.com>"
LABEL version="${VERSION-${TARGETPLATFORM}}"

RUN apk update && \
	apk add git make --virtual .build-deps && \
    pip install -U pip && \
    pip3 install requests && \
	git clone https://github.com/jarun/ddgr.git && \
    cd ddgr && \
    git checkout ${VERSION-v1.1} && \
    make install && \
    cd .. && \
    rm -Rf ddgr && \
    apk del git --purge .build-deps

ENV BROWSER=
ENTRYPOINT ["ddgr"]
